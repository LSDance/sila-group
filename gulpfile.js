'use strict';

let gulp = require('gulp'),
    sass = require('gulp-sass'),
    twig = require('gulp-twig'),
    autoprefixer = require('gulp-autoprefixer');

const langs = ['ru', 'en'];
let htmlRu = function () {
    return gulp.src(["./src/html/*.twig", "!./src/html/wrapper.twig"])
        .pipe(twig({
            data: {
                data: require('./data/data_ru.js'),
                version: 'v2.2'
            }
        }))
        .pipe(gulp.dest("./dist/"))
};
let htmlEn = function () {
    return gulp.src(["./src/html/*.twig", "!./src/html/wrapper.twig"])
        .pipe(twig({
            data: {
                data: require('./data/data_en.js'),
                version: 'v2.2'
            }
        }))
        .pipe(gulp.dest("./dist/en"))
};

let css = function () {
    return gulp.src(["./src/sass/*.scss"])
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(autoprefixer({
            cascade: true
        }))
        .pipe(gulp.dest("./dist/css/"))
        .pipe(gulp.dest("./dist/en/css"))
};

let js = function () {
    return gulp.src(["./src/js/*.js"])
        .pipe(gulp.dest("./dist/js/"))
        .pipe(gulp.dest("./dist/en/js"))
};

let img = function () {
    return gulp.src(["./src/img/*"])
        .pipe(gulp.dest("./dist/img/"))
        .pipe(gulp.dest("./dist/en/img/"))
};

let fonts = function () {
    return gulp.src(["./src/fonts/**/*"])
        .pipe(gulp.dest("./dist/fonts/"))
        .pipe(gulp.dest("./dist/en/fonts/"))
};

let watch = function() {
    gulp.watch("./src/sass/**/*", css);
    gulp.watch("./src/html/*.twig", htmlRu);
    gulp.watch("./src/html/*.twig", htmlEn);
    gulp.watch("./src/img/**/*", img);
    gulp.watch("./src/js/**/*", js);
};


exports.htmlEn = htmlEn;
exports.htmlRu = htmlRu;
exports.css = css;
exports.js = js;
exports.img = img;
exports.fonts = fonts;
exports.build = gulp.series(htmlRu, htmlEn ,css,js,img,fonts);
exports.watch = gulp.series(htmlRu, htmlEn ,css,js,img,fonts,watch);

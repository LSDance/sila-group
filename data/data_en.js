module.exports = {
    lang: 'en',
    langInt: 'en',
    meta: {
        title: 'Sila Group',
        description: 'Sila Group',
        keywords: 'Sila Group'
    },
    nav: [
        {
            title: 'about the holding',
            link: '#block1'
        },
        {
            title: 'our business',
            link: '#block2'
        },
        {
            title: 'activities',
            link: '#block3'
        },
        {
            title: 'partners',
             link: '#block4'
        },
        {
            title: 'contacts',
             link: '#block5'
        },
    ],

    sectionBanner: {
        title: '<span class="section-banner__title--marked">SilaGroup</span> Russianholding company  <br>established to <span class="section-banner__title--marked">start</span> and <span class="section-banner__title--marked">develop</span> Russian <span class="section-banner__title--marked">innovative high tech companies</span> and social projects'
    },
    sectionHand: {
        text: 'Efficient and profitable investments are top priorities for Sila Group. We have been implementing large-scale IT projects, successfully investing in the development and high tech.'
    },
    sectionBusiness: {
    title: 'Business<br><span class="section__title--marked">SilaGroup</span>',
    business: [
        {
            title: 'Radius wifi',
            img: 'img/business-wifi',
            logo: 'img/logo-wifi',
            text: 'Radius OOO is a Russian company <br>which provides WiFi-based <br>services.<br>It works with more than 1500 <br>clients and 80 partners <br>in Russia and CIS.<br>Radius OOO offers <br>software <br>for telephone number<br>authorization in public WiFi<br>networks, as well as advanced<br>marketing capabilities.',
            url: 'https://radiuswifi.ru/',
        },
        {
            title: 'Show Me VR',
            img: 'img/business-viarium',
            logo: 'img/logo-showmevr',
            text: 'A service for quickly creating presentations, conferences, and training programs in virtual reality. VR presentations are an innovative technology that creates a wow effect and helps attract new customers attention to a product or service.',
            url: 'https://showmevr.online/',
        },
        {
            title: 'JACK–IT',
            img: 'img/business-jack',
            logo: 'img/logo-jack',
            text: 'Jack–IT develops, <br>integrates and supports<br>complex IT solutions for<br>effective business development.<br>They create smart IT platforms <br>to work with ease.',
            url: 'https://jack-it.ru/',
        },
        {
            title: 'PRESENT FOR THE ANGEL',
            img: 'img/business-podarok',
            logo: 'img/logo-podarok',
            text: 'The Present For The Angel<br>Charity Project aims to help<br>cerebral palsy children. Since its origination, the Fund has helped more than 5,000 children through 4 charitable programs.',
            url: 'https://podarokangelu.com/',
        },
        {
            title: 'VINCI',
            img: 'img/business-vinci',
            logo: 'img/logo-vinci',
            text: 'Vinci is the first messenger, that keeps your private life private. Vinci has more communication options than traditional messengers: \n' +
                'Secret passcode let to hide and protect separate chats;\n' +
                'Spoofing passcodes let to imitate an access to all chats!',
            url: 'https://vinci.my',
        },
        {
            title: 'TORG-PROM',
            img: 'img/business-tp',
            logo: 'img/logo-tp',
            text: 'Wholesale distribution <br>of own-produced vegetables for borsch (onions, beet, cabbage, potato, carrot), various salads, Chinese and red cabbage, zucchini.',
            url: 'http://torg-prom.com/',
        },
        {
                title: 'VC SILA (VENTURE <br>CAPITAL INVEST)',
                img: 'img/business-vc',
                logo: 'img/logo-vc',
                text: 'VC Sila is a company that represents a private innovation ecosystem. Under one brand, SILA combines acceleration programs for startups, investments, as well as large-scale projects with corporations and the state to create an innovative environment that meets global challenges of the time.',
                url: 'https://vcsila.com/',
            },
    ]
},
sectionDirection: {
    title: 'Activities<br><span class="section__title--marked"> </span>',
    direction: [
        {
            name: {
                0: 'Information',
                1: 'Technology',
            }
        },
        {
            name: 'Telecommunications'
        },
        {
            name: {
                0: 'Development ',
    1: 'and extension of infrastructure'
}
},
    {
        name: {
            0: 'Commercial',
            1: 'activities'
        }
    }
]
},
sectionPartners: {
    title: 'Partners<br><span class="section__title--marked">Sila Group</span>',
    partners: [
            {
                img: 'img/partner-1',
                url: 'http://altegronet.ru/'
            },
            {
                img: 'img/partner-2',
                url: 'https://www.asus.com/'
            },
            {
                img: 'img/partner-3',
                url: 'https://www.cisco.com/c/ru_ru/index.html'
            },
            {
                img: 'img/partner-4',
                url: 'https://virgilsecurity.com/'
            },
            {
                img: 'img/partner-5',
                url: 'https://mikrotik.ru/'
            },
            {
                img: 'img/partner-6',
                url: 'https://www.htc.com/ru/'
            },
            {
                img: 'img/partner-7',
                url: 'https://dokkur.com/'
            },
            {
                img: 'img/partner-8',
                url: 'https://www.oculus.com/'
            },
            // {
            //     img: 'img/partner-9',
            //     url: ''
            // },
            {
                img: 'img/partner-10',
                url: 'https://hkibfa.io/'
            },
            // {
            //     img: 'img/partner-11',
            //     url: 'https://radiuswifi.ru/'
            // },
            {
                img: 'img/partner-12',
                url: ''
            }
        ]
},
sectionContacts: {
    title: 'Contacts',
    address: '36 Lyusinovskaya street bld 1, Glass House Business Center<br>Moscow 115093, Russia<br>',
    phone: '+7 (495) 132-48-22'
},
copyright: '©2019 Sila Group. All rights reserved.'

};


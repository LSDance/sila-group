module.exports = {
    lang: 'ru',
    langInt: 'ru',
    meta: {
        title: 'Sila Group',
        description: 'Sila Group',
        keywords: 'Sila Group'
    },
    nav: [
        {
            title: 'о холдинге.',
            link: '#block1'
        },
        {
            title: 'наши бизнесы.',
            link: '#block2'
        },
        {
            title: 'направления.',
            link: '#block3'
        },
        {
            title: 'партнеры.',
            link: '#block4'
        },
        {
            title: 'контакты.',
            link: '#block5'
        },
    ],

    sectionBanner: {
        title: '<span class="section-banner__title--marked">SilaGroup</span> – российская <span class="br br--desctop"></span> холдинговая компания, образованная <br>с целью <span class="section-banner__title--marked">создания</span> и <span class="section-banner__title--marked">развития</span><span class="br br--desctop"></span> <span class="section-banner__title--marked">инновационных высокотехнологичных <span class="br br--desctop"></span>компаний</span> и социальных проектов'
    },
    sectionHand: {
        text: 'Эффективные и прибыльные <span class="br br--desctop"></span>инвестиции являются приоритетом <span class="br br--desctop"></span>для Sila Group. Мы реализуем масштабные <span class="br br--desctop"></span>проекты в информационных технологиях, <span class="br br--desctop"></span>успешно инвестируем в девелопмент <span class="br br--desctop"></span>и высокие технологии.'
    },
    sectionBusiness: {
        title: 'Бизнесы<br><span class="section__title--marked">SilaGroup</span>',
        business: [
            {
                title: 'Radius wifi',
                img: 'img/business-wifi',
                logo: 'img/logo-wifi',
                text: 'Российская компания, <br>предоставляющая сервис <br>на основе технологии WiFi, <br>работающая с более чем 1500 <br>клиентов и 80 партнерами <br>по городам России и СНГ. <br>ООО «Радиус» предоставляет <br>программное обеспечение <br>для авторизации по номеру <br>телефона в общественных  WiFi- <br>сетях, а также дополнительные <br>маркетинговые возможности.',
                url: 'https://radiuswifi.ru/',
            },
            {
                title: 'Show Me VR',
                img: 'img/business-viarium',
                logo: 'img/logo-showmevr',
                text: 'Сервис для быстрого создания презентаций, конференций и обувающих программ в виртуальной реальности. VR презентации - это инновационная технология, создающая wow эффект и помогающая привлечь внимание новых клиентов к продукту или услуге.',
                 url: 'https://showmevr.online/',
            },
            {
                title: 'JACK–IT',
                img: 'img/business-jack',
                logo: 'img/logo-jack',
                text: 'Jack–IT – это разработка, <br>интеграция и поддержка <br>комплексных IT-решений для <br>эффективного развития бизнеса. <br>Создают комфортные IT-платформы <br>для максимально удобной работы.',
                 url: 'https://jack-it.ru/',
            },
            {
                title: 'ПОДАРОК АНГЕЛУ',
                img: 'img/business-podarok',
                logo: 'img/logo-podarok',
                text: 'Благотворительный проект <br>«Подарок Ангелу» для помощи <br>детям с ДЦП. За годы <br>существования фонд оказал <br>помощь более 5000 детей по 4 <br>благотворительным программам.',
                 url: 'https://podarokangelu.com/',
            },
            {
                title: 'VINCI',
                img: 'img/business-vinci',
                logo: 'img/logo-vinci',
                text: 'Vinci - первый мессенджер, помогающий держать свою частную жизнь в тайне. В Vinci больше возможностей для общения, чем в традиционных средствах связи: Доступ к скрытым чатам возможен благодаря Секретному паролю. А ввод Подменного пароля имитирует открытие всей переписки.',
                 url: 'https://vinci.my',
            },
            {
                title: 'ТОРГ–ПРОМ',
                img: 'img/business-tp',
                logo: 'img/logo-tp',
                text: 'Осуществляет оптовые <br>поставки овощей борщевой группы <br>собственного производства, <br>различных видов салатов, <br>пекинской и краснокочанной <br>капусты, кабачков.',
                 url: 'http://torg-prom.com/',
            },
             {
                title: 'VC SILA (VENTURE <br>CAPITAL INVEST)',
                img: 'img/business-vc',
                logo: 'img/logo-vc',
                text: 'VC Sila – компания, которая представляет собой частную<br /> инновационную экосистему. Под одним брендом SILA объединяет<br /> акселерационные программы для стартапов, инвестиции,<br /> а также масштабные проекты с корпорациями и государством<br /> по созданию инновационной среды, отвечающей на глобальные<br /> вызовы времени.',
                 url: 'https://vcsila.com/',
            },
        ]
    },
    sectionDirection: {
        title: 'Направления<br><span class="section__title--marked">деятельности</span>',
        direction: [
            {
                name: {
                    0: 'Информационные',
                    1: 'технологии',
                }
            },
            {
                name: 'Телекоммуникации'
            },
            {
                name: {
                    0: 'Девелопмент ',
                    1: 'и развитие ',
                    2: 'инфраструктуры'
                }
            },
            {
                name: {
                    0: 'Торговая',
                    1: 'деятельность'
                }
            }
        ]
    },
    sectionPartners: {
        title: 'Партнёры<br><span class="section__title--marked">Sila Group</span>',
        partners: [
            {
                img: 'img/partner-1',
                url: 'http://altegronet.ru/'
            },
            {
                img: 'img/partner-2',
                url: 'https://www.asus.com/'
            },
            {
                img: 'img/partner-3',
                url: 'https://www.cisco.com/c/ru_ru/index.html'
            },
            {
                img: 'img/partner-4',
                url: 'https://virgilsecurity.com/'
            },
            {
                img: 'img/partner-5',
                url: 'https://mikrotik.ru/'
            },
            {
                img: 'img/partner-6',
                url: 'https://www.htc.com/ru/'
            },
            {
                img: 'img/partner-7',
                url: 'https://dokkur.com/'
            },
            {
                img: 'img/partner-8',
                url: 'https://www.oculus.com/'
            },
            // {
            //     img: 'img/partner-9',
            //     url: ''
            // },
            {
                img: 'img/partner-10',
                url: 'https://hkibfa.io/'
            },
            // {
            //     img: 'img/partner-11',
            //     url: 'https://radiuswifi.ru/'
            // },
            {
                img: 'img/partner-12',
                url: ''
            }
        ]
    },
    sectionContacts: {
        title: 'Контакты',
        address: '115093, Россия,<br>Москва, БЦ Glass House, <br>ул. Люсиновская 36с1',
        phone: '+7 (495) 132-48-22'
    },
    copyright: '©2019 Sila Group. All rights reserved.'

};

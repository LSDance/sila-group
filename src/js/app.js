$('.navbar__toggler').click(function (e) {
    e.preventDefault();

    $('.header').toggleClass( 'header--open', '' );

    return false;
});

$('.anchor').click(function (e) {
    e.preventDefault();

    var hash = $(this).attr("href");

    if($(window).width() < 768) {
        $('.navbar__toggler').click()
    }

    $("html, body").animate({
        scrollTop: $(hash).offset().top
    }, 1000);

    return false;
});


var offset = $(window).width() >= 1200 ? 200 : 40;
$(window).scroll(function (e) {
    let showMe = $('.showMe');
    for (let i = 0; i < showMe.length; i++) {
        if ($(window).scrollTop() >= showMe.eq(i).offset().top - $(window).height() + offset) {
            showMe.eq(i).addClass('showMe--show')
        }

    }
});
